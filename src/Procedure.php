<?php

declare(strict_types=1);

namespace ponci_berlin\phpbaercode;

use CBOR\ListObject;
use CBOR\UnsignedIntegerObject;
use DateTime;

class Procedure
{
    private int $type;
    private DateTime $time;
    

    public function __construct(int $type, DateTime $time)
    {
        $this->type = $type;
        $this->time = $time;
    }

    public function encode_unserialsed_cbor()
    {
        return new ListObject([
            UnsignedIntegerObject::create($this->type),
            UnsignedIntegerObject::create($this->time->getTimestamp())
        ]);
    }


}

?>
