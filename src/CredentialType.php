<?php

namespace ponci_berlin\phpbaercode;

class CredentialType
{
    const Unspecified = 0; // used for testing
    const Vaccine = 1;
    const NegativeTest = 2;
    const Invalid = 3; // used for enum validation
}