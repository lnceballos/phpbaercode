<?php

declare(strict_types=1);

namespace ponci_berlin\phpbaercode\cose;

use CBOR\ByteStringObject;
use CBOR\ListObject;
use CBOR\TextStringObject;
use phpseclib3\Crypt\EC;
use phpseclib3\Math;

use ErrorException;

use function PHPUnit\Framework\throwException;

/**
 * Signature is an abstraction of the COSE signature object, described here:
 * https://tools.ietf.org/html/rfc8152#section-4.1
 *
 * For now we hardcode ES512, if this ever becomes a general purpose COSE library, expand.
 */
class Signature
{
    const CONTEXT = "Signature";
    // Hardcoded for now, as we only support the p521 curve
    const P512_CURVE_KEY_BYTES_SIZE = 66;

    public COSEHeaders $headers;
    public string $signature;
    private EC\PrivateKey $key;
    private SignMessage $message;

    public function __construct(SignMessage $message, EC\PrivateKey $key, string $kid)
    {
        $this->headers = new COSEHeaders();
        $this->signature = "";
        $this->key = $key;
        $this->message = $message;
        array_push($message->signatures, $this);
        $this->headers->set_alg("ES512");
        $this->headers->set_kid($kid);
    }

    /**
     * sign signs the message with the key and writes it to signature.
     */
    public function sign()
    {
        $sig_digest = $this->build_signature_digest();
        $raw_signature = $this->key->withSignatureFormat('raw')->withHash('sha512')->sign($sig_digest);
        $sig = $this->i20sp($raw_signature["r"], self::P512_CURVE_KEY_BYTES_SIZE);
        $sig .= $this->i20sp($raw_signature["s"], self::P512_CURVE_KEY_BYTES_SIZE);
        $this->signature = $sig;
    }

    private function i20sp(Math\BigInteger $b, int $l): string
    {
        $octets = $b->toBytes();
        $octlen = mb_strlen($octets, '8bit');
        if ($octlen > $l) {
            throw new ErrorException("integer too large");
        }
        $result = '';
        if ($octlen < $l) {
            $times = $l - $octlen;
            for ($i = 0; $i < $times; $i++) {
                $result .= pack('C', 0);
            }
        }
        $result .= $octets;
        return $result;
    }

    private function build_signature_digest(): string
    {
        $sig_struct = new ListObject([
            new TextStringObject($this::CONTEXT),
            new ByteStringObject($this->message->headers->encode_protected()),
            new ByteStringObject($this->headers->encode_protected()),
            new ByteStringObject(""),
            new ByteStringObject($this->message->payload)
        ]);
        return (string)$sig_struct;

    }

    public function encode_cbor(): ListObject
    {
        if (strlen($this->signature) == 0) {
            throw new ErrorException("Not serialising unsigned message.");
        }

        return new ListObject([
            new ByteStringObject($this->headers->encode_protected()),
            $this->headers->encode_unprotected(),
            new ByteStringObject($this->signature),
        ]);
    }

}
