<?php 

namespace ponci_berlin\phpbaercode\cose;

use CBOR\CBORObject;
use CBOR\TagObject as Base;
use CBOR\Utils;

class SignTag extends Base
{
    // Redefine this to be able to access it here.
    private const MAJOR_TYPE = 0b110;

    public static function getTagId(): int
    {
        return 98;
    }

    public static function createFromLoadedData(int $additionalInformation, ?string $data, CBORObject $object): Base
    {
        return new self($additionalInformation, $data, $object);
    }

    public static function create(CBORObject $object): Base
    {
        return new self(98, null, $object);
    }

    /**
     * __toString overrides the normal TagObject functionality, because it doesn't
     * deal well with tags that are higher than 23.
     * TODO: submit MR to upstream.
     */
    public function __toString(): string
    {
        $result = chr(self::MAJOR_TYPE << 5 | 24) . chr($this->additionalInformation);
        if (null !== $this->data) {
            $result .= $this->data;
        }
        $result .= (string) $this->object;

        return $result;
    }

    public function getNormalizedData(bool $ignoreTags = false)
    {
        return Utils::hexToString($this->object->getValue());
    }
}
?>
