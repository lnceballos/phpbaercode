<?php

declare(strict_types=1);

use ponci_berlin\phpbaercode\cose\Encrypt0Message;
use PHPUnit\Framework\TestCase;

final class Encrypt0MessageIntegrationTest extends TestCase
{
    /**
     * @doesNotPerformAssertions
     */
    public function testEncrypt0Generation(): void
    {
        $key = openssl_random_pseudo_bytes(16);
        $msg = new Encrypt0Message("test12356", "aabbccddeeff");
        $msg->encrypt_aesgcm($key);
        $cbor_encoded = $msg->encode_cbor();
        $cbor_array = unpack('C*', $cbor_encoded);
        $scratch_dir = getenv("SCRATCH_DIR");
        file_put_contents("$scratch_dir/php_encrypt0.cbor", bin2hex($cbor_encoded));
        file_put_contents("$scratch_dir/php_key.hex", bin2hex($key));
    }
}
?>
